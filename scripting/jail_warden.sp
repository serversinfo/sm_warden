#pragma semicolon 1 

#include <sourcemod>
#include <sdktools>
#include <csgo_colors>
#include <lastrequest>
#include <t2lrcfg>
#include <sm_jail_redie>
#include <hlstatsX_adv>
#pragma newdecls required
#include <warden>
#define VERSION "1.1.18"

//stock void debugMessage(const char[] message, any ...)
//{
//	char szMessage[256], szPath[PLATFORM_MAX_PATH];
//	BuildPath(Path_SM, szPath, sizeof(szPath), "logs/debug_warden.txt");
//	VFormat(szMessage, sizeof(szMessage), message, 2);
//	LogToFile(szPath, szMessage);
//}
//#define dbgMsg(%0) debugMessage(%0)

/*Основная работа плагина*/
int warden;								//Тут запоминаем ID командующего
bool vote_cmd;							//Идет ли голосовние за КМД
bool noblock;							//Включен ли noBlock
int g_offsCollisionGroup		= -1;	//Для noBlock; 2 - noblock, 5 - block, 17 - расталкивать

int all_votes 					= 0;
int timer_sec					= 0;
int warden_vots[MAXPLAYERS+1]	= 0;

Handle h_Timer					= INVALID_HANDLE;
Handle h_Timer_NoBlock;
Handle mp_solid_teammates       = null;

Menu coremenu;					//Главное меню
Menu mCTmenu;					//КТ меню
Menu votemenu;					//Меню голосования
/*Глобальные ф-ции*/
Handle g_fward_onBecome 		= INVALID_HANDLE;
Handle g_fward_onRemove			= INVALID_HANDLE;

/*Моделька*/
char GlowLightPath[PLATFORM_MAX_PATH] = "materials/sprites/glow04.vmt";

public Plugin myinfo =
{
	name	= "[CS:GO] JAil Warden (Командующий)",
	author	= "Darkeneez & ShaRen",
	version	= VERSION,
	url		= "vk.com/darkeneez97"
}

public void OnPluginStart()
{
	/*Для NoBlock'а*/
	g_offsCollisionGroup = FindSendPropInfo("CBaseEntity", "m_CollisionGroup");
	if (g_offsCollisionGroup == -1)
		SetFailState("[Warden] Failed to get offset for CBaseEntity::m_CollisionGroup.");
	
	mp_solid_teammates = FindConVar( "mp_solid_teammates" );		// чтобы не сталкиваться с противником
	RegAdminCmd("sm_noblock", Command_NoBlock, ADMFLAG_BAN);

	/*Команды*/
	AddCommandListener(CallBack, "say_team");
	AddCommandListener(CallBack, "say");
	AddCommandListener(CallBack, "say2");
	
	//RegConsoleCmd("sm_spray", create_spray);
	
	/*Создадим глобальное меню*/
	CreateCoreMenu();
	CreateCTMenu();
	
	/*Глобальные ф-ции*/
	g_fward_onBecome = CreateGlobalForward("warden_OnWardenCreated", ET_Ignore, Param_Cell);
	g_fward_onRemove = CreateGlobalForward("warden_OnWardenRemoved", ET_Ignore, Param_Cell);
	
	/*События*/
	HookEvent("player_team", eV_PlayerTeam);
	HookEvent("player_death", playerDeath);
	HookEvent("round_start", round_start, EventHookMode_PostNoCopy);
	//dbgMsg("public void OnPluginStart() --------------------");
}

public Action Command_NoBlock(int iClient, int args)
{
	int iCTs;
	for (int i=1; i<MAXPLAYERS; i++) {
		if (IsClientInGame(i) && GetClientTeam(i) == 3)
			iCTs++;
	}
	iCTs ? PrintToChat(iClient, "Можно включать только когда нету КТ") : NoBlock(iClient);
}

public void OnMapStart()
{
	PrecacheModel(GlowLightPath);

	AddFileToDownloadsTable("materials/sprites/glow04.vmt");
	AddFileToDownloadsTable("materials/sprites/glow04.vtf");
	//dbgMsg("[КМД] ~ OnMapStart");
}

stock void playmusic(char[] sound)
{
	for(int i=1; i<=MaxClients; i++)
		if(IsClientInGame(i) && !IsFakeClient(i))
			ClientCommand(i, sound);
}

public int OnStartLR(int PrisonerIndex, int GuardIndex, int type)
{
	if (noblock) {
		for (int i = 1; i <= MaxClients; i++) {
			if ( !IsClientInGame(i) || !IsPlayerAlive(i) || IsPlayerGhost(i) )
				continue;
			SetEntData(i, g_offsCollisionGroup, 17, 4, true);
		}
		h_Timer_NoBlock = CreateTimer(1.5, Timer_NoBlock);
		CGOPrintToChatAll("{RED}[КМД] {GREEN}no-block автоматически выключен");
		//dbgMsg("[КМД] OnStartLR ~ no-block автоматически выключен");
		noblock = false;
	}
}

public void OnClientDisconnect(int client)
{
	warden_vots[client] = 0;
	if (client == warden) {
		ExitWarden(client);
		playmusic("play ui/armsrace_demoted.wav");
		CGOPrintToChatAll("{RED}[КМД] {GREEN}Командир {BLUE}%N{GREEN} вышел с сервера! Командиром станет первый по списку КТ!", client);
	}
}

public Action playerDeath(Handle event, char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid")); 
	warden_vots[client] = 0;
	if(client == warden) {
		CGOPrintToChatAll("{RED}[КМД]{GREEN}Командир {BLUE}%N{GREEN} умер! Командиром станет первый по списку КТ!", client);
		if (GetClientMenu(client))
			CancelClientMenu(client);
		warden = 0;
		MaxFragsWarden();
	}
}

public Action round_start(Handle event, char[] name, bool silent) 
{
	warden = 0;
	noblock = false;		// вроде как сервер сам делает всем m_CollisionGroup стандартным в начале раунда
	SetConVarBool( mp_solid_teammates, true );

	if (h_Timer != INVALID_HANDLE) {
		KillTimer(h_Timer);
		h_Timer = INVALID_HANDLE;
	}
	if (GetTeamClientCount(3) == 1) {
		for (int i=1; i<MAXPLAYERS; i++)
			if (IsValidCT(i))
				BecomeWarden(i, true);
	} else if (!warden && GetTeamClientCount(2) > 2) {
		votemenu = new Menu(vote_menu);
		votemenu.SetTitle("Выберите командующего\n");
		SetMenuExitButton(votemenu, false);
		char StR_Id[15];
		char StR_Name[MAX_NAME_LENGTH]; 
		votemenu.AddItem("-1", "Не голосовать");
		for (int i=1; i<=MaxClients; i++) {
			warden_vots[i] = 0;
			if (IsClientInGame(i) && GetClientTeam(i) == 3 && GetClientName(i, StR_Name, MAX_NAME_LENGTH)) {
				IntToString(GetClientUserId(i), StR_Id, sizeof(StR_Id));
				votemenu.AddItem(StR_Id, StR_Name);
			}
		}
		for (int i = 1; i <= MaxClients; i++)
			if (IsClientInGame(i) && GetClientTeam(i) == 2 && !IsFakeClient(i))
				votemenu.Display(i, 10);
		all_votes = 0;
		timer_sec = 12;
		vote_cmd = true;
		h_Timer = CreateTimer(1.0, Timer_Func, _, TIMER_REPEAT);
	}
	//dbgMsg("[КМД] public Action:round_start");
}

public int vote_menu(Menu menu, MenuAction action, int client, int option)
{	//голосование за КМД
	if (action != MenuAction_Select)
		return;

	char StR_Id[15];
	if (!GetMenuItem(menu, option, StR_Id, sizeof(StR_Id)))
		return;

	int target_userid = StringToInt(StR_Id);
	int target = GetClientOfUserId(target_userid);
	if (target_userid == -1) {}
	else if (target) {
		all_votes++;
		warden_vots[target]++;
		CGOPrintToChat(target, "{GREEN}| {RED}%N {GREEN}проголосовал за тебя |", client);
		ShowHintTimerTop3();
	} else CGOPrintToChat(client,"{RED}[КМД] {GREEN}Игрок не найден");
}

public Action Timer_Func (Handle timer_f)
{	// TIMER_REPEAT 1 sec
	if (--timer_sec > 0) {
		ShowHintTimerTop3();
		return Plugin_Continue;
	}
	vote_cmd = false;
	if (h_Timer != INVALID_HANDLE) {	// Время истекло, голосование окончено, останавливаем TIMER_REPEAT
		KillTimer(h_Timer);
		h_Timer = INVALID_HANDLE;
	}

	if (votemenu != INVALID_HANDLE)
		CloseHandle(votemenu);
 
	if (!all_votes) {
		PrintHintTextToAll("Никто не проголосовал");
		MaxFragsWarden();
	} else {
		int target = 0;
		for (int i=1; i<=MaxClients; i++)
			if (warden_vots[i] > warden_vots[target])
				target = i;
		if (target && IsClientInGame(target) && !IsFakeClient(target) && GetClientTeam(target) == 3 && IsPlayerAlive(target) && !IsPlayerGhost(target)) {//проверка проверка проверка :D
			CGOPrintToChatAll("{GREEN}╔═══\n║ Голосование на командира завершено!\n║ За игрока {BLUE}%N {GREEN}проголосовало {DEFAULT}%d/%d {GREEN}чел.\n╚═══\n", target, warden_vots[target], all_votes);
			BecomeWarden(target, false);
			playmusic("play ui/achievement_earned.wav");
		} else CGOPrintToChatAll("{RED}[КМДъ {GREEN}Игрок за КТ не найден, введите !w или !c чтобы стать командиром.");
	}
	return Plugin_Stop;
}

void ShowHintTimerTop3()
{
	int top3[3];
	for (int i=1; i<=MaxClients; i++) {
		if (warden_vots[i] > warden_vots[top3[0]]) {
			top3[2] = top3[1];
			top3[1] = top3[0];
			top3[0] = i;
		} else if (warden_vots[i] > warden_vots[top3[1]]) {
			top3[2] = top3[1];
			top3[1] = i;
		} else if (warden_vots[i] > warden_vots[top3[2]])
			top3[2] = i;
	}
	char name0[78]; char name1[78]; char name2[78];
	if (top3[0]) {
		GetClientName(top3[0], name0, sizeof(name0));
		Format(name0, sizeof(name0),"%s<font color='#468499'>[%i]</font>", name0, warden_vots[top3[0]]);
		if (top3[1]) {
			GetClientName(top3[1], name1, sizeof(name1));
			Format(name1, sizeof(name1),"%s<font color='#f6546a'>[%i]</font>", name1, warden_vots[top3[1]]);
			if (top3[2]) {
				GetClientName(top3[2], name2, sizeof(name2));
				Format(name2, sizeof(name2),"%s<font color='#20b2aa'>[%i]</font>", name2, warden_vots[top3[2]]);
			}
		}
	}
	char names[244];
	Format(names, sizeof(names), "<font color='#008000'>%s %s %s</font>", name0, name1, name2);
	PrintHintTextToAll("<font color='#ccff00'size='30'>Осталось: %d сек.</font>\n%s", timer_sec, names);
}

public Action eV_PlayerTeam(Handle event, const char[] name, bool db)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	warden_vots[client] = 0;
	if (client == warden)
		ExitWarden(client);
}

public Action CallBack(int client, char[] command, int args)
{
	char message[12];
	GetCmdArg(1, message, sizeof(message));
	
	if (StrEqual(message, "!кмд") || StrEqual(message, "!cmd") || StrEqual(message, "!w") || StrEqual(message, "!W") || StrEqual(message, "!ц") || StrEqual(message, "!Ц") || StrEqual(message, "!c") || StrEqual(message, "!с") || StrEqual(message, "!warden")) { //Если игрок пытаются зайти в меню
		if (GetClientTeam(client) != 3) {			//если он не КТ
			if (!warden)
				CGOPrintToChat(client,"{RED}[КМД] {GREEN}Только КТ могут использовать эту команду");
			else CGOPrintToChat(client,"{RED}[КМД] {GREEN}Командиром является {BLUE}%N{GREEN}.", warden);
			return Plugin_Continue;
		}
		if (client == warden) {
			ShowMainMenu(client);					// если он КМД, то просто открываем ему меню
			return Plugin_Continue;
		} else if (!warden) {						// если вообще нет КМД
			BecomeWarden(client, true);
			playmusic("play ui/csgo_ui_crate_item_scroll.wav");
			return Plugin_Continue;
		} else {
			CGOPrintToChat(client,"{RED}[КМД] {GREEN}Уже есть командир {BLUE}%N{GREEN}.", warden);		// Пишем ему кто на самом деле КМД
			if (IsPlayerAlive(client) && GetClientTeam(client) == 3)
				mCTmenu.Display(client, MENU_TIME_FOREVER);
			return Plugin_Continue;
		}
	} else if(StrEqual(message, "!uw") || StrEqual(message, "!unwarden") || StrEqual(message, "!uc") || StrEqual(message, "!uncommander") || StrEqual(message, "!выход")) { //Если уходит с поста командующего
		if (GetClientTeam(client) != 3) {			// если он не КТ
			CGOPrintToChat(client,"{RED}[КМД] {GREEN}Только КТ могут использовать эту команду");
			return Plugin_Continue;
		}
		if (client == warden) {						// если он КМД то он покидает пост
			ExitWarden(client);
			playmusic("play ui/armsrace_demoted.wav");
			return Plugin_Continue;
		} else {
			CGOPrintToChat(client,"{RED}[КМД] {GREEN}Уже есть командир {BLUE}{1}{GREEN}.");		// пишем что он и не был КМД
			return Plugin_Continue;
		}
	}
	return Plugin_Continue;
}

/*Основное меню*/
stock void CreateCoreMenu()
{
	/*Main Core Menu*/
	coremenu = new Menu(CoreHandle);
	coremenu.SetTitle("Servers-Info.Ru меню КМД");
	
	coremenu.AddItem("CountTs",		"✔ Пересчитать заключенных (!check)");		//case 0:
	coremenu.AddItem("TPos",		"☄ Найти заключенных (!tpos)");				//case 1:
	coremenu.AddItem("NoBLock",		"☮ NoBlock Вкл./Выкл");						//case 2:
	coremenu.AddItem("TeamGames",	"㋛ Мини-Игры (плагин)");					//case 3:
	coremenu.AddItem("OpenJails",	"[◄|►] Открыть камеры");					//case 4:
	coremenu.AddItem("Games",		"シ Простые игры");							//case 5:
	coremenu.AddItem("Respawn",		"(×̯×) Возродить убитых (!resp)");			//case 6:
	coremenu.AddItem("Cap",			"¯\\(ツ)_/¯ Предложить сдаться (!cap)");		//case 7:
	coremenu.AddItem("ShopMenu",	"$$$ Оружие (!shopmenu)");					//case 8:
	coremenu.AddItem("Ball",		"◍ Мячик (ball)");							//case 8:
	//coremenu.AddItem("Ghost",		"(•̪●) Призраки(!dm)");						//case 9:
	coremenu.AddItem("Exit",		"✖ Выход", ITEMDRAW_CONTROL);				//case 10:
}

public int CoreHandle(Menu menu, MenuAction action, int client, int option)
{
	if (action == MenuAction_Cancel || action == MenuAction_End)
		return;
		
	/* If an option was selected, tell the client about the item. */
	else if (action == MenuAction_Select) {
		switch(option) {
			case 0: {
				FakeClientCommand(client, "sm_check");
				ShowMainMenu(client);
			}
			case 1: {
				FakeClientCommand(client, "sm_tpos");
				ShowMainMenu(client);
			}
			case 2: {
				NoBlock(client);
				ShowMainMenu(client);
			}
			case 3: FakeClientCommand(client, "sm_tg");
			case 4: SmOpenJail();
			case 5: FakeClientCommand(client, "sm_games");
			case 6: FakeClientCommand(client, "sm_resp");
			case 7: FakeClientCommand(client, "sm_cap");
			case 8: FakeClientCommand(client, "sm_shopmenu");
			case 9: FakeClientCommand(client, "ball");
			case 10: {
				ExitWarden(client);
				playmusic("play ui/armsrace_demoted.wav");
			}
		}
	}
}

stock void SmOpenJail()
{
	ServerCommand("sm_open_jail");
	playmusic("play buttons/lever2.wav");
	CGOPrintToChatAll("{RED}[КМД] {BLUE}%N {GREEN}открыл джайлы", warden);
	for (int i=1; i<MAXPLAYERS; i++)
		if(IsClientInGame(i) && IsPlayerAlive(i) && GetClientTeam(i) == 2 && GetClientPlayedTime(i) < 3600)
			CGOPrintToChat(i, "{RED}ПРЕДУПРЕЖДЕНИЕ {GREEN}Не выходите из камеры пока не услышите приказа на выход");
}

stock void ShowMainMenu(int client)
{
	if (client == warden && warden)
		coremenu.Display(client, MENU_TIME_FOREVER);
}

/*КТ меню*/
stock void CreateCTMenu()
{
	/*Main Core Menu*/
	mCTmenu = new Menu(CTHandle);
	mCTmenu.SetTitle("Меню охранника (вы не кмд)");
	
	mCTmenu.AddItem("CountTs",		"✔ Пересчитать заключенных (!check)");		//case 0:
	mCTmenu.AddItem("TPos",			"☄ Найти заключенных (!tpos)");				//case 1:
	mCTmenu.AddItem("Respawn",		"(×̯×) Возродить убитых (!resp)");			//case 2:
	mCTmenu.AddItem("Cap",			"¯\\(ツ)_/¯ Предложить сдаться (!cap)");		//case 3:
	mCTmenu.AddItem("ShopMenu",		"$$$ Оружие (!shopmenu)");					//case 4:
	mCTmenu.AddItem("Ball",			"◍ Мячик (ball)");							//case 5:
}

public int CTHandle(Menu menu, MenuAction action, int client, int option)
{
	if (action == MenuAction_Cancel || action == MenuAction_End)
		return;
		
	/* If an option was selected, tell the client about the item. */
	else if (action == MenuAction_Select) {
		switch(option) {
			case 0: {
				FakeClientCommand(client, "sm_check");
				ShowCTMenu(client);
			}
			case 1: {
				FakeClientCommand(client, "sm_tpos");
				ShowMainMenu(client);
			}
			case 2: FakeClientCommand(client, "sm_resp");
			case 3: FakeClientCommand(client, "sm_cap");
			case 4: FakeClientCommand(client, "sm_shopmenu");
			case 5: FakeClientCommand(client, "ball");
		}
	}
}

stock void ShowCTMenu(int client)
{
	if (IsValidCT(client))
		mCTmenu.Display(client, MENU_TIME_FOREVER);
}

/*Глобальные ф-ции (Для остальных плагинов)*/
public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	CreateNative("warden_exist", Native_ExistWarden);
	CreateNative("warden_iswarden", Native_IsWarden);
	CreateNative("warden_isnoblock", Native_IsNoBlock);
	RegPluginLibrary("warden");
	return APLRes_Success;
}

/*Если игрок командующий*/
public int Native_IsWarden(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	
	if(client > 0 && !IsClientInGame(client))
		ThrowNativeError(SP_ERROR_INDEX, "Client index %i is invalid", client);
	
	return (client == warden && client > 0) ? true : false;
}

/*Если игрок вышел*/
public int Native_ExistWarden(Handle plugin, int numParams)
{
	return (warden) ? true : false;
}

/*Если включен ноблок*/
public int Native_IsNoBlock(Handle plugin, int numParams)
{
	return (noblock) ? true : false;
}

public void Forward_OnWardenCreation(int client)
{
	Call_StartForward(g_fward_onBecome);
	Call_PushCell(client);
	Call_Finish();
}

public void Forward_OnWardenRemoved(int client)
{
	Call_StartForward(g_fward_onRemove);
	Call_PushCell(client);
	Call_Finish();
}
/*Ф-ции командира*/

/*Пробуем стать командующим*/
stock void BecomeWarden(int client, bool ShowMenu)
{
	if (IsValidCT(client) && !vote_cmd) {
		CGOPrintToChatAll("{RED}[КМД] {GREEN}Командиром стал {BLUE}%N", client);
		warden = client;
		Forward_OnWardenCreation(client);						//Глобальная ф-ция для других плагинов
		PrintHintTextToAll("Командиром стал <font color='#0000ff' size='30'>%N</font>", client);

		for (int i=1; i<MAXPLAYERS; i++)
			if(IsClientInGame(i) && IsPlayerAlive(i) && GetClientTeam(i) == 2 && GetClientPlayedTime(i) < 3600) {
				CGOPrintToChat(i, "{RED}ВНИМАНИЕ {GREEN}Внимательно слушайте игрока {BLUE}%N{GREEN}, он стал командиром", warden);
				CGOPrintToChat(i, "{GREEN}Можете устроить бунт напав на КТ, но вас за это могут убить.");
				CGOPrintToChat(i, "{GREEN}Или будьте послушным, играйте в игры и доживите до LR");
			}
		if(ShowMenu)
			ShowMainMenu(client);
		else PrintHintText(client, "Ты стал командиром <font color='#0000ff' size='30'>!w - меню КМД</font>");
	} else if(vote_cmd) {
		if (h_Timer != null)
			CGOPrintToChat(client,"{RED}[КМД] {GREEN}Идет голосование! Осталось %i сек. дождитесь окончания.", timer_sec);
		else {
			timer_sec = 0;
			vote_cmd = false;
			if (IsValidCT(client))
				BecomeWarden(client, true);
		}
	}
}

/*Вышел из командира*/
stock void ExitWarden(int client)
{
	CGOPrintToChatAll("{RED}[КМД] {BLUE}%N{GREEN} покинул пост командира.", client);
	if (GetClientMenu(client))
		CancelClientMenu(client);
	warden = 0;
	Forward_OnWardenRemoved(client);
}

stock void NoBlock(int client)
{
	if (!noblock && !IsLrActivated()) {
		for (int i = 1; i <= MaxClients; i++) {
			if ( !IsClientInGame(i) || !IsPlayerAlive(i) || IsPlayerGhost(i))
				continue;
			//SetEntData(i, g_offsCollisionGroup, 2, 4, true);
			SetEntData(i, g_offsCollisionGroup, 5, 4, true);
			SetConVarBool( mp_solid_teammates, false );
		}
		if (h_Timer_NoBlock != INVALID_HANDLE) {
			KillTimer(h_Timer_NoBlock);
			h_Timer_NoBlock = INVALID_HANDLE;
		}
		CGOPrintToChatAll("{RED}[КМД] {GREEN}NO Block включен");
		//dbgMsg("[КМД] ~ NO Block включен");
		playmusic("play ui/csgo_ui_page_scroll.wav");
		noblock = true;
	} else if (noblock) {
		for (int i = 1; i <= MaxClients; i++) {
			if ( !IsClientInGame(i) || !IsPlayerAlive(i) || IsPlayerGhost(i) )
				continue;
			SetEntData(i, g_offsCollisionGroup, 17, 4, true);
		}
		h_Timer_NoBlock = CreateTimer(1.3, Timer_NoBlock);
		CGOPrintToChatAll("{RED}[КМД] {GREEN}NO Block выключен");
		//dbgMsg("[КМД] ~ NO Block выключен");
		playmusic("play buttons/button2.wav");
		noblock = false;
	} else {
		CGOPrintToChat(client,"{RED}[КМД] {GREEN}Вы не можете включать no-block во время ЛР");
		//dbgMsg("[КМД] ~ Вы не можете включать no-block во время ЛР");
	}
}

public Action Timer_NoBlock(Handle timer)
{
	for (int i = 1; i <= MaxClients; i++) {
		if ( !IsClientInGame(i) || !IsPlayerAlive(i) || IsPlayerGhost(i))
			continue;
		SetEntData(i, g_offsCollisionGroup, 5, 4, true);
		SetConVarBool( mp_solid_teammates, true );
	}
	h_Timer_NoBlock = INVALID_HANDLE;
	//dbgMsg("[КМД] ~ Timer_NoBlock");
}


stock void MaxFragsWarden()
{
	/*Выбираем другого у кого больше фрагов*/
	int target;
	int max = -1;
	int frags;
	
	for (int i = 1; i <= MaxClients; i++) {
		if (!IsValidCT(i))
			continue;
		frags = GetClientFrags(i);
		
		if (max < frags) {
			max = frags;
			target = i;
		} else if (max == frags)
			switch (GetRandomInt(0,1)) {
				case 0: {
					max = frags;
					target = i;
				}
			}
	}
	if (target > 0) {
		BecomeWarden(target, false);
		playmusic("play ui/armsrace_level_up.wav");
	} else CGOPrintToChatAll("Некого назначить КМД");
}

stock void GetLookPosition_f(int client, float aim_Position[3])
{ 
	float EyePosition[3]; 
	float EyeAngles[3];
	Handle h_trace; 
	
	GetClientEyePosition(client, EyePosition); 
	GetClientEyeAngles(client, EyeAngles); 
	h_trace = TR_TraceRayFilterEx(EyePosition, EyeAngles, MASK_SOLID, RayType_Infinite, GetLookPos_Filter_F, client); 
	TR_GetEndPosition(aim_Position, h_trace); 
	CloseHandle(h_trace); 
} 

public bool GetLookPos_Filter_F(int ent, int mask, int client) 
{ 
	return client != ent; 
}

stock bool IsValidCT(int client)
{
	if (client <= 0 || !IsClientInGame(client) || !IsPlayerAlive(client) || IsPlayerGhost(client) || GetClientTeam(client) != 3 || IsFakeClient(client))
		return false;
	return true;
}
