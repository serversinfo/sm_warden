#if defined _warden_included
  #endinput
#endif
#define _warden_included

/*********************************************************
 * Checks if any warden exist
 *
 * @true on match , false if not
 *********************************************************/
native bool warden_exist();

/*********************************************************
 * Проверяет включен ли но блок
 *
 * @true on match , false if not
 *********************************************************/
native bool warden_isnoblock();

/*********************************************************
 * returns if client is warden
 *
 * @param client		The client to run the check on
 * @true on match, false if not		
 *********************************************************/
native bool warden_iswarden(int client);

/*********************************************************
 * Set a client as warden
 *
 * @param client		The client to set as warden
 * @NoReturn
 *********************************************************/
//native void warden_set(int client);

/*********************************************************
 * Removes the current warden if he exists
 *
 * @param client		The warden client to remove
 * @NoReturn	
 *********************************************************/
//native void warden_remove(int client);